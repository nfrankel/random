package com.sap.ps.expertise.yaas;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
public class RandomController {

    private final Random random;

    @Autowired
    public RandomController(Random random) {
        this.random = random;
    }

    @RequestMapping(value = "/get", method = GET)
    public int random(@RequestParam(value="limit", defaultValue="100") int limit ) {
        return random.nextInt(limit);
    }
}
